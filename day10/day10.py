#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """position=< 9,  1> velocity=< 0,  2>
position=< 7,  0> velocity=<-1,  0>
position=< 3, -2> velocity=<-1,  1>
position=< 6, 10> velocity=<-2, -1>
position=< 2, -4> velocity=< 2,  2>
position=<-6, 10> velocity=< 2, -2>
position=< 1,  8> velocity=< 1, -1>
position=< 1,  7> velocity=< 1,  0>
position=<-3, 11> velocity=< 1, -2>
position=< 7,  6> velocity=<-1, -1>
position=<-2,  3> velocity=< 1,  0>
position=<-4,  3> velocity=< 2,  0>
position=<10, -3> velocity=<-1,  1>
position=< 5, 11> velocity=< 1, -2>
position=< 4,  7> velocity=< 0, -1>
position=< 8, -2> velocity=< 0,  1>
position=<15,  0> velocity=<-2,  0>
position=< 1,  6> velocity=< 1,  0>
position=< 8,  9> velocity=< 0, -1>
position=< 3,  3> velocity=<-1,  1>
position=< 0,  5> velocity=< 0, -1>
position=<-2,  2> velocity=< 2,  0>
position=< 5, -2> velocity=< 1,  2>
position=< 1,  4> velocity=< 2,  1>
position=<-2,  7> velocity=< 2, -2>
position=< 3,  6> velocity=<-1, -1>
position=< 5,  0> velocity=< 1,  0>
position=<-6,  0> velocity=< 2,  0>
position=< 5,  9> velocity=< 1, -2>
position=<14,  7> velocity=<-2,  0>
position=<-3,  6> velocity=< 2, -1>"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 0)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 0)
		pass

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		pos_part, vel_part = line.strip().split("> velocity=<")
		pos_part = pos_part[10:].strip()
		vel_part = vel_part[:-1].strip()
		pos = tuple(int(c.strip().strip(",")) for c in pos_part.split())
		vel = tuple(int(c.strip().strip(",")) for c in vel_part.split())
		o.append(((pos[1], pos[0]), (vel[1], vel[0])))
	return o
	#return readin.int_list_input(inp)
	#return readin.chars_input(inp)

def part1(inp, max_size = 9): # 7 for 
	vals = inp
	while True:
		new_vals = []
		max_y = -math.inf
		min_y = math.inf
		for pos, vel in vals:
			new_vals.append(((pos[0] + vel[0], pos[1] + vel[1]), vel))
			max_y = max(max_y, pos[0] + vel[0])
			min_y = min(min_y, pos[0] + vel[0])
		vals = new_vals
		if (max_y - min_y) <= max_size:
			break
	return prettyprint(vals)

def prettyprint(vals):
	y_min = min(v[0][0] for v in vals)
	y_max = max(v[0][0] for v in vals)
	x_min = min(v[0][1] for v in vals)
	x_max = max(v[0][1] for v in vals)
	positions = set(v[0] for v in vals)
	s = ""
	for y in range(y_min, y_max + 1):
		row = ""
		for x in range(x_min, x_max + 1):
			if (y, x) in positions:
				row += "#"
			else:
				row += " "
		s += (row + "\n")
	return s


def part2(inp):
	t = 0
	vals = inp
	while True:
		t += 1
		new_vals = []
		max_y = -math.inf
		min_y = math.inf
		for pos, vel in vals:
			new_vals.append(((pos[0] + vel[0], pos[1] + vel[1]), vel))
			max_y = max(max_y, pos[0] + vel[0])
			min_y = min(min_y, pos[0] + vel[0])
		vals = new_vals
		if (max_y - min_y) < 10:
			return t

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
