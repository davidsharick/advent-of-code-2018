#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """42"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), "21,61")
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 0)
		pass

def parse_input(inp, p2mode = False):
	return readin.int_list_input(inp)[0]
	#return readin.chars_input(inp)

def part1(inp):
	serial = inp
	grid = []
	for y in range(300):
		row = []
		for x in range(300): # X and Y are 1 less than their "real" value here
			rack_id = x + 11
			power_level = rack_id * (y + 1)
			power_level += serial
			power_level *= rack_id
			power_level = (power_level // 100) % 10
			power_level -= 5
			row.append(power_level)
		grid.append(row)
	best = (0, (-1, -1))
	for y in range(298):
		for x in range(298):
			s = sum(sum(grid[y2][x2] for x2 in range(x, x + 3)) for y2 in range(y, y + 3))
			if s > best[0]:
				best = (s, (y, x))
	return str(1 + best[1][1]) + "," + str(1 + best[1][0])

def part2(inp):
	serial = inp
	grid = []
	for y in range(300):
		row = []
		for x in range(300): # X and Y are 1 less than their "real" value here
			rack_id = x + 11
			power_level = rack_id * (y + 1)
			power_level += serial
			power_level *= rack_id
			power_level = (power_level // 100) % 10
			power_level -= 5
			row.append(power_level)
		grid.append(row)
	best = (0, (-1, -1, -1))
	prefix_sums = []
	for y in range(300):
		row = []
		curr = 0
		for x in range(300): # X and Y are 1 less than their "real" value here
			curr += grid[y][x]
			row.append(curr)
		prefix_sums.append(row)
	for size in range(1, 301):
		for y in range(301 - size):
			for x in range(301 - size):
				s = 0
				x_max = x + size - 1
				for y2 in range(y, y + size):
					if x > 0:
						s += (prefix_sums[y2][x_max] - prefix_sums[y2][x - 1])
					else:
						s += prefix_sums[y2][x_max]
				if s > best[0]:
					best = (s, (y, x, size)) # not 144,58,9
	return str(1 + best[1][1]) + "," + str(1 + best[1][0]) + "," + str(best[1][2])

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
