#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """initial state: #..#.#..##......###...###

...## => #
..#.. => #
.#... => #
.#.#. => #
.#.## => #
.##.. => #
.#### => #
#.#.# => #
#.### => #
##.#. => #
##.## => #
###.. => #
###.# => #
####. => #"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 325)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 0)
		pass

def parse_input(inp, p2mode = False):
	rules = Counter()
	mapper = lambda x: 1 if x == "#" else 0
	for idx, line in enumerate(inp):
		if idx == 0:
			state = line.strip().split()[2].strip()
			state = list(map(mapper, state))
		elif idx > 1:
			parts = line.strip().split(" => ")
			value = sum(((2 ** (5 - i)) * mapper(v)) for i, v in enumerate(parts[0]))
			rules[value] = mapper(parts[1])
	return state, rules

def part1(inp):
	idx_0 = 0
	state, rules = inp
	for _ in range(20):
		new_state = []
		must_cont = False
		for idx in range(-2, len(state) + 2):
			new_value = change_value(state, rules, idx)
			if idx == -2 and new_value == 1:
				idx_0 += 2
				must_cont = True
				new_state.append(new_value)
			elif idx == -1 and must_cont:
				new_state.append(new_value)
			elif idx == -1 and new_value == 1:
				idx_0 += 1
				new_state.append(new_value)
			else:
				if idx >= 0:
					new_state.append(new_value)
		state = new_state
		#prettyprint(state, idx_0)
	s = 0
	for i, v in enumerate(state):
		if v:
			s += (i - idx_0)
	return s

def change_value(total_state, rules, index):
	s = 0
	for exp in range(5):
		new_idx = index + exp - 2
		if new_idx < 0 or new_idx >= len(total_state):
			val = 0
		else:
			val = total_state[new_idx]
		s += ((2 ** (5 - exp)) * val)
	return rules[s]

def prettyprint(state, idx_0):
	s = " " * idx_0
	s += "0\n"
	s += "".join("#" if v else "." for v in state)
	print(s, idx_0)	

def part2(inp):
	idx_0 = 0
	state, rules = inp
	i = 0
	l = []
	while idx_0 < 2:
		new_state = []
		must_cont = False
		for idx in range(-2, len(state) + 2):
			new_value = change_value(state, rules, idx)
			if idx == -2 and new_value == 1:
				idx_0 += 2
				must_cont = True
				new_state.append(new_value)
			elif idx == -1 and must_cont:
				new_state.append(new_value)
			elif idx == -1 and new_value == 1:
				idx_0 += 1
				new_state.append(new_value)
			else:
				if idx >= 0:
					new_state.append(new_value)
		if new_state[1:-1] == state:
			#pr(i, "".join("#" if x == 1 else "." for x in state))
			# Steady state found, from here it will just push the indices forward 1 each step
			current_sum = 0
			num_1s = 0
			for i2, v in enumerate(state):
				if v:
					current_sum += (i2 - idx_0)
					num_1s += 1
			future_steps = (50000000000 - i)
			return current_sum + (future_steps * num_1s)
		state = new_state
		i += 1
	return 0
	

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
