#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.direction as direc
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

# Requires adding an extra space at the end of each line in the input to prevent \ from clobbering \n when reading in input

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """/->-\         
|   |  /----\ 
| /-+--+-\  | 
| | |  | v  | 
\-+-/  \-+--/ 
  \------/    
"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = """/>-<\   
|   |   
| /<+-\ 
| | | v 
\>+</ | 
  |   ^ 
  \<->/ 
"""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), "7,3")
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), "6,4")
		pass

def parse_input(inp, p2mode = False):
	carts = []
	for y, line in enumerate(inp):
		for x, char in enumerate(line):
			if char in "<>v^":
				if char == '<':
					direction = direc.LEFT
				elif char == '>':
					direction = direc.RIGHT
				elif char == 'v':
					direction = direc.DOWN
				elif char == '^':
					direction = direc.UP
				else:
					assert False
				carts.append((y, x, 0, direction))
	return readin.grid_input(inp, mapping = data.IdentityDict({">": "-", "<": "-", "^": "|", "v": "|", "\\": "\\"})), carts
	#return readin.int_list_input(inp)
	#return readin.chars_input(inp)

def part1(inp):
	grid, carts = inp
	max_width = len(grid[0])
	x = 0
	while True:
		x += 1
		carts.sort(key = lambda cart: cart[0] * max_width + cart[1])
		new_carts = []
		new_positions = set()
		curr_positions = set((c[0], c[1]) for c in carts)
		for cart in carts:
			y, x, cycle, direction = cart
			grid_object = grid[y][x]
			new_direction = None
			if grid_object in "-|":
				pass
			elif grid_object == "/":
				if direction == direc.UP or direction == direc.DOWN:
					new_direction = direc.turn_right(direction)
				elif direction == direc.LEFT or direction == direc.RIGHT:
					new_direction = direc.turn_left(direction)
				else:
					assert False
			elif grid_object == "\\":
				if direction == direc.UP or direction == direc.DOWN:
					new_direction = direc.turn_left(direction)
				elif direction == direc.LEFT or direction == direc.RIGHT:
					new_direction = direc.turn_right(direction)
				else:
					assert False
			elif grid_object == "+":
				if cycle == 0:
					# turn left
					new_direction = direc.turn_left(direction)
				elif cycle == 1:
					# go straight
					pass
				elif cycle == 2:
					# turn right
					new_direction = direc.turn_right(direction)
				cycle += 1
				cycle %= 3
			else:
				assert False, f"Invalid grid object `{grid_object}`"
			if new_direction is not None:
				direction = new_direction	
			curr_positions.remove((y, x))
			new_pos = (y + direction[0], x + direction[1])
			if new_pos in new_positions or new_pos in curr_positions:
				return str(new_pos[1]) + "," + str(new_pos[0])
			new_positions.add(new_pos)
			new_carts.append((new_pos[0], new_pos[1], cycle, direction))
		carts = new_carts
	
	return 0

def part2(inp):
	grid, carts = inp
	max_width = len(grid[0])
	x = 0
	while True:
		x += 1
		carts.sort(key = lambda cart: cart[0] * max_width + cart[1])
		new_carts = []
		new_positions = set()
		curr_positions = set((c[0], c[1]) for c in carts)
		skip_carts = set()
		for cart in carts:
			y, x, cycle, direction = cart
			if (y, x) in skip_carts:
				continue
			grid_object = grid[y][x]
			new_direction = None
			if grid_object in "-|":
				pass
			elif grid_object == "/":
				if direction == direc.UP or direction == direc.DOWN:
					new_direction = direc.turn_right(direction)
				elif direction == direc.LEFT or direction == direc.RIGHT:
					new_direction = direc.turn_left(direction)
				else:
					assert False
			elif grid_object == "\\":
				if direction == direc.UP or direction == direc.DOWN:
					new_direction = direc.turn_left(direction)
				elif direction == direc.LEFT or direction == direc.RIGHT:
					new_direction = direc.turn_right(direction)
				else:
					assert False
			elif grid_object == "+":
				if cycle == 0:
					# turn left
					new_direction = direc.turn_left(direction)
				elif cycle == 1:
					# go straight
					pass
				elif cycle == 2:
					# turn right
					new_direction = direc.turn_right(direction)
				cycle += 1
				cycle %= 3
			else:
				assert False, f"Invalid grid object `{grid_object}`"
			if new_direction is not None:
				direction = new_direction	
			curr_positions.remove((y, x))
			new_pos = (y + direction[0], x + direction[1])
			if new_pos in new_positions or new_pos in curr_positions:
				skip_carts.add(new_pos)

			else:
				new_positions.add(new_pos)
				new_carts.append((new_pos[0], new_pos[1], cycle, direction))
		carts = list(filter(lambda cart: (cart[0], cart[1]) not in skip_carts, new_carts))
		if len(carts) == 1:
			return f"{carts[0][1]},{carts[0][0]}"
	return 0

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
