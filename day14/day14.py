#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """2018"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = """94142"""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 5941429882)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 2018)
		pass

def parse_input(inp, p2mode = False):
	return readin.int_list_input(inp)[0]
	#return readin.chars_input(inp)

def part1(inp):
	recipes = [3, 7]
	elf1_idx = 0
	elf2_idx = 1
	goal = inp
	while True:
		s = recipes[elf1_idx] + recipes[elf2_idx]
		s2 = str(s)
		for c in s2:
			recipes.append(int(c))
		elf1_idx += recipes[elf1_idx] + 1
		elf2_idx += recipes[elf2_idx] + 1
		elf1_idx %= len(recipes)
		elf2_idx %= len(recipes)
		if goal + 9 < len(recipes):
			return int("".join(str(c) for c in recipes[-10:]))
	return 0

# Below 20279773

def part2(inp):
	recipes = [3, 7]
	elf1_idx = 0
	elf2_idx = 1
	goal = list(int(c) for c in str(inp))
	while True:
		s = recipes[elf1_idx] + recipes[elf2_idx]
		s2 = str(s)
		for c in s2:
			recipes.append(int(c))
		elf1_idx += recipes[elf1_idx] + 1
		elf2_idx += recipes[elf2_idx] + 1
		elf1_idx %= len(recipes)
		elf2_idx %= len(recipes)
		if recipes[-len(goal):] == goal:
			return len(recipes) - len(goal)
		elif recipes[-len(goal) - 1:-1] == goal:
			return len(recipes) - len(goal) - 1
	return 0

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
