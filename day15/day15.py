#!/usr/bin/env python3

from collections import defaultdict, Counter
from copy import deepcopy
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """#######
#.G...#
#...EG#
#.#.#G#
#..G#E#
#.....#
#######"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = """#######
#G..#E#
#E#E.E#
#G.##.#
#...#E#
#...E.#
#######"""
		self.test2 = parse_input(self.testinput2.split("\n"))

		self.testinput3 = """#######
#E..EG#
#.#G.E#
#E.##E#
#G..#.#
#..E#.#
#######"""
		self.test3 = parse_input(self.testinput3.split("\n"))

		self.testinput4 = """#######
#E.G#.#
#.#G..#
#G.#.G#
#G..#.#
#...E.#
#######"""
		self.test4 = parse_input(self.testinput4.split("\n"))

		self.testinput5 = """#######
#.E...#
#.#..G#
#.###.#
#E#G#G#
#...#G#
#######"""
		self.test5 = parse_input(self.testinput5.split("\n"))

		self.testinput6 = """#########
#G......#
#.E.#...#
#..##..G#
#...##..#
#...#...#
#.G...G.#
#.....G.#
#########"""
		self.test6 = parse_input(self.testinput6.split("\n"))

		#self.testinput2 = self.testinput#""""""
		#self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		return
		self.assertEqual(part1(self.test), 27730)
		self.assertEqual(part1(self.test2), 36334)
		self.assertEqual(part1(self.test3), 39514)
		self.assertEqual(part1(self.test4), 27755)
		self.assertEqual(part1(self.test5), 28944)
		self.assertEqual(part1(self.test6), 18740)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 4988)
		self.assertEqual(part2(self.test3), 31284)
		self.assertEqual(part2(self.test4), 3478)
		self.assertEqual(part2(self.test5), 6474)
		self.assertEqual(part2(self.test6), 1140)
		pass

def parse_input(inp, p2mode = False):
	return readin.grid_input(inp)
	#return readin.int_list_input(inp)
	#return readin.chars_input(inp)

def part1(inp):
	area = inp
	actors = [] # Tuple of id int, is_goblin bool, (y, x) posn, hp int
	curr_id = 0
	max_width = len(area[0])
	full_size = len(area) * len(area[0])
	ranges = ((0, len(area)), (0, len(area[0])))
	for y, line in enumerate(area):
		for x, char in enumerate(line):
			if char == 'E':
				actors.append([curr_id, False, (y, x), 200])
				curr_id += 1
				area[y][x] = 1
			elif char == 'G':
				actors.append([curr_id, True, (y, x), 200])
				curr_id += 1
				area[y][x] = 1
			elif char == '.':
				area[y][x] = 0
			elif char == "#":
				area[y][x] = 1
			else:
				pr(char)
				assert False
	
	round_num = 0
	while True:
		actors.sort(key = lambda actor: actor[2][0] * max_width + actor[2][1])
		i = 0
		while i < len(actors): # Concurrent modification issue workaround
			actor = actors[i]
			ns = grid.get_neighbors(2, actor[2], ranges, corners = False) # Pre-sorted
			target_found = False
			possible_targets = []
			for actor2 in actors:
				if actor2[2] in ns and actor2[1] ^ actor[1]:
					# Target acquired
					target_found = True
					possible_targets.append(actor2)
				if target_found:
					possible_targets.sort(key = lambda a: (full_size * a[3]) + (max_width * a[2][0]) + a[2][1]) # TODO more discerning
					target = possible_targets[0]
			if target_found:
				destination = actor[2]
				pass
			else: # find the place to move to
				enemies = filter(lambda a: a[1] ^ actor[1], actors)
				bests = [math.inf, []]
				path_func, node_func = generate_validators(actor[2], area)
				distances, paths = grid.grid_dijkstra(area, actor[2], path_func, node_validator = node_func, corners = False)
				seen_enemy = False
				for enemy in enemies:
					seen_enemy = True
					points = filter(lambda p: area[p[0]][p[1]] == 0, grid.get_neighbors(2, enemy[2], ranges, corners = False))
					for pt in points:
						try:
							if distances[pt] < bests[0]:
								bests[0] = distances[pt]
								bests[1] = [pt]
							elif distances[pt] == bests[0]:
								bests[1].append(pt)
						except:
							pass
				if not seen_enemy:
					return sum(actor[3] for actor in actors) * round_num
				if bests[0] == math.inf:
					destination = actor[2]
				else:
					destination = sorted(bests[1], key = lambda posn: posn[0] * max_width + posn[1])[0]
					while paths[destination] != actor[2]:
						destination = paths[destination]
			assert abs(actor[2][0] - destination[0]) + abs(actor[2][1] - destination[1]) <= 1 # Sanity checking

			# Move
			area[actor[2][0]][actor[2][1]] = 0
			actor.remove(actor[2])
			actor.insert(2, destination)
			actor[2] = destination
			area[actor[2][0]][actor[2][1]] = 1

			# Find target if not already found
			if not target_found:
				ns = grid.get_neighbors(2, actor[2], ranges, corners = False) # Pre-sorted
				possible_targets = []
				for actor2 in actors:
					if actor2[2] in ns and actor2[1] ^ actor[1]:
						# Target acquired
						target_found = True
						possible_targets.append(actor2)
					possible_targets.sort(key = lambda a: (full_size * a[3]) + (max_width * a[2][0]) + a[2][1]) # TODO more discerning
				if target_found:
					target = possible_targets[0]
				else:
					target = None
			if target is not None:
				# kill them
				target[3] -= 3
				if target[3] <= 0:
					area[target[2][0]][target[2][1]] = 0
					if actors.index(target) < i:
						i -= 1
					actors.remove(target)
			i += 1
		round_num += 1
		#print(f"Round number is: after {round_num}")
		#prettyprint(area, actors)
	return 0

# Below 226894

def generate_validators(node, area):
	yb, xb = node
	path_func = lambda p1, p2: 1 if (sum(area[y][x] for y, x in (p1, p2)) == 0) or (p1 == node) or (p2 == node) else -1
	node_func = lambda n: (area[n[0]][n[1]] == 0) or n == node
	return path_func, node_func

def prettyprint(area, actors):
	elves = set(actor[2] for actor in filter(lambda a: not a[1], actors))
	goblins = set(actor[2] for actor in filter(lambda a: a[1], actors))
	for y, line in enumerate(area):
		s = ""
		for x, pos in enumerate(line):
			if (y, x) in elves:
				assert pos == 1
				s += "E"
			elif (y, x) in goblins:
				assert pos == 1
				s += "G"
			elif pos == 1:
				s += "#"
			else:
				s += "."
		print(s)
	print(actors)

def part2(inp):
	area = deepcopy(inp)
	actors = [] # Tuple of id int, is_goblin bool, (y, x) posn, hp int
	curr_id = 0
	max_width = len(area[0])
	full_size = len(area) * len(area[0])
	ranges = ((0, len(area)), (0, len(area[0])))
	num_elves = 0
	for y, line in enumerate(area):
		for x, char in enumerate(line):
			if char == 'E':
				actors.append([curr_id, False, (y, x), 200])
				curr_id += 1
				area[y][x] = 1
				num_elves += 1
			elif char == 'G':
				actors.append([curr_id, True, (y, x), 200])
				curr_id += 1
				area[y][x] = 1
			elif char == '.':
				area[y][x] = 0
			elif char == "#":
				area[y][x] = 1
			else:
				pr(char)
				assert False
	actors_backup = deepcopy(actors)
	area_backup = deepcopy(area)
	elf_attack = 4
	while True:
		round_num = 0
		simulation_ended = False
		actors = deepcopy(actors_backup)
		area = deepcopy(area_backup)
		while not simulation_ended:
			actors.sort(key = lambda actor: actor[2][0] * max_width + actor[2][1])
			i = 0
			while i < len(actors): # Concurrent modification issue workaround
				actor = actors[i]
				ns = grid.get_neighbors(2, actor[2], ranges, corners = False) # Pre-sorted
				target_found = False
				possible_targets = []

				# Search for a nearby target
				for actor2 in actors:
					if actor2[2] in ns and actor2[1] ^ actor[1]:
						# Target acquired
						target_found = True
						possible_targets.append(actor2)
					if target_found:
						possible_targets.sort(key = lambda a: (full_size * a[3]) + (max_width * a[2][0]) + a[2][1]) # TODO more discerning
						target = possible_targets[0]
				if target_found:
					destination = actor[2]
					pass
				else: # find the place to move to

					enemies = filter(lambda a: a[1] ^ actor[1], actors)
					bests = [math.inf, []]
					path_func, node_func = generate_validators(actor[2], area)
					distances, paths = grid.grid_dijkstra(area, actor[2], path_func, node_validator = node_func, corners = False)
					seen_enemy = False
					for enemy in enemies:
						seen_enemy = True
						points = filter(lambda p: area[p[0]][p[1]] == 0, grid.get_neighbors(2, enemy[2], ranges, corners = False))
						for pt in points:
							try:
								if distances[pt] < bests[0]:
									bests[0] = distances[pt]
									bests[1] = [pt]
								elif distances[pt] == bests[0]:
									bests[1].append(pt)
							except:
								pass
					if not seen_enemy:
						if (not actor[1]) and len(actors) == num_elves:
							return sum(actor[3] for actor in actors) * round_num
						simulation_ended = True
						break
					if bests[0] == math.inf:
						destination = actor[2]
					else:
						destination = sorted(bests[1], key = lambda posn: posn[0] * max_width + posn[1])[0]
						while paths[destination] != actor[2]:
							destination = paths[destination]
				assert abs(actor[2][0] - destination[0]) + abs(actor[2][1] - destination[1]) <= 1 # Sanity checking

				# Move
				area[actor[2][0]][actor[2][1]] = 0
				actor.remove(actor[2])
				actor.insert(2, destination)
				actor[2] = destination
				area[actor[2][0]][actor[2][1]] = 1

				# Find target if not already found
				if not target_found:
					ns = grid.get_neighbors(2, actor[2], ranges, corners = False) # Pre-sorted
					possible_targets = []
					for actor2 in actors:
						if actor2[2] in ns and actor2[1] ^ actor[1]:
							# Target acquired
							target_found = True
							possible_targets.append(actor2)
						possible_targets.sort(key = lambda a: (full_size * a[3]) + (max_width * a[2][0]) + a[2][1]) # TODO more discerning
					if target_found:
						target = possible_targets[0]
					else:
						target = None
				if target is not None:
					# kill them
					if actor[1]:
						target[3] -= 3
					else:
						target[3] -= elf_attack
					if target[3] <= 0:
						area[target[2][0]][target[2][1]] = 0
						if actors.index(target) < i:
							i -= 1
						actors.remove(target)
				i += 1
			round_num += 1
			#print(f"Round number is: after {round_num} with attack {elf_attack} {num_elves}")
			#prettyprint(area, actors)
		elf_attack += 1
	return 0

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
