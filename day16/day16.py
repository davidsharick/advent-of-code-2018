#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 0)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 0)
		pass

def parse_input(inp, p2mode = False):
	opcodes_mode = True
	o = []
	o2 = []
	consec = 0
	for idx, line in enumerate(inp):
		if not opcodes_mode:
			o2.append(list(int(c) for c in line.strip().split()))
			continue
		if len(line.strip()) == 0:
			consec += 1
			if consec >= 3:
				opcodes_mode = False
				if not p2mode:
					return o
			pass
		elif line.startswith("Before"):
			consec = 0
			curr = [ast.literal_eval(line.strip()[7:])]
			next_idx = idx + 1
		elif idx == next_idx:
			curr.append(list(int(c) for c in line.strip().split()))
		elif line.startswith("After"):
			curr.append(ast.literal_eval(line.strip()[6:]))
			o.append(curr)
	return o, o2
	#return readin.int_list_input(inp)
	#return readin.chars_input(inp)

def addr(args, registers):
	return registers[args[0]] + registers[args[1]]

def addi(args, registers):
	return registers[args[0]] + args[1]

def mulr(args, registers):
	return registers[args[0]] * registers[args[1]]

def muli(args, registers):
	return registers[args[0]] * args[1]

def banr(args, registers):
	return registers[args[0]] & registers[args[1]]

def bani(args, registers):
	return registers[args[0]] & args[1]

# Apparently not necessary for part 1!
def borr(args, registers):
	return registers[args[0]] | registers[args[1]]

def bori(args, registers):
	return registers[args[0]] | args[1]

def setr(args, registers):
	return registers[args[0]]

def seti(args, registers):
	return args[0]

def gtri(args, registers):
	return 1 if registers[args[0]] > args[1] else 0

def gtir(args, registers):
	return 1 if args[0] > registers[args[1]] else 0

def gtrr(args, registers):
	return 1 if registers[args[0]] > registers[args[1]] else 0

def eqri(args, registers):
	return 1 if registers[args[0]] == args[1] else 0

def eqir(args, registers):
	return 1 if args[0] == registers[args[1]] else 0

def eqrr(args, registers):
	return 1 if registers[args[0]] == registers[args[1]] else 0

opcodes = [addr, addi, mulr, muli, banr, bani, borr, bori, setr, seti, gtri, gtir, gtrr, eqri, eqir, eqrr]

def part1(inp):
	o = 0
	for before_reg, args, after_reg in inp:
		c = 0
		for opcode in opcodes:
			if after_reg[args[3]] == opcode(args[1:], before_reg):
				c += 1
				if c >= 3:
					o += 1
					break
	return o

def part2(inp):
	test_data, prog = inp
	o = 0
	possibilities = []
	for opcode in range(16):
		possibilities.append(opcodes.copy())
	for before_reg, args, after_reg in test_data:
		for idx, opcode in enumerate(possibilities[args[0]]):
			if after_reg[args[3]] != opcode(args[1:], before_reg):
				possibilities[args[0]].remove(opcode)
	real_opcodes = {}
	while len(real_opcodes) < 16:
		for idx, p in enumerate(possibilities):
			if len(p) == 1:
				real_opcodes[idx] = p[0]
				found = p[0]
				for p2 in possibilities:
					if found in p2:
						p2.remove(found)
	registers = [0, 0, 0, 0]
	for op in prog:
		registers[op[3]] = real_opcodes[op[0]](op[1:], registers)
	return registers[0]

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
