#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """x=495, y=2..7
y=7, x=495..501
x=501, y=3..7
x=498, y=2..4
x=506, y=1..2
x=498, y=10..13
x=504, y=10..13
y=13, x=498..504"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 57)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 0)
		pass

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		parts = line.strip().split(", ")
		second_range = [int(c) for c in parts[1].split("=")[1].split("..")]
		final_range = range(second_range[0], second_range[1] + 1)
		range_length = len(final_range)
		first_value = int(parts[0].split("=")[1])
		if parts[0][0] == "x":
			o.append((final_range, [first_value] * range_length))
		elif parts[0][0] == "y":
			o.append(([first_value] * range_length, final_range))
		else:
			assert False
		pass
	return o
	#return readin.int_list_input(inp)
	#return readin.chars_input(inp)

def part1(inp):
	max_pos = [0, 500, math.inf]
	vein_positions = set()
	for vein in inp:
		for y, x in zip(vein[0], vein[1]):
			max_pos = [max(y, max_pos[0]), max(x, max_pos[1]), min(y, max_pos[2])]
			vein_positions.add((y, x))
	space = []
	for y in range(max_pos[0] + 1):
		row = []
		for x in range(max_pos[1] + 1):
			if (y, x) in vein_positions:
				row.append(1)
			else:
				row.append(0)
		row.append(0)
		space.append(row)
	water_source = (0, 500)
	# 0: empty
	# 1: clay
	# 2: water block
	# -1: water was here, but not solid
	anything_filled = True
	while anything_filled:
		anything_filled = False
		seen = set()
		# Send "pulses" of water to ~~deal 60 base power special water damage~~ flow until fully settling one row per split flow
		water_heads = [list(water_source)]
		while len(water_heads) > 0: # Tick for individual pulse
			new_water_heads = []
			for wh in water_heads:
				# tick each once
				y, x = wh
				# Move down if possible
				while y + 1 < len(space) and space[y + 1][x] <= 0:
					wh[0] += 1
					space[y + 1][x] = -1
					y += 1
				now_x = x
				if y + 1 >= len(space):
					continue
				blocked_count = 0
				while space[y][now_x] <= 0 and space[y + 1][now_x] >= 1:
					# We can't move down: we need to go sideways
					now_x += 1
					if space[y][now_x] >= 1:
						# blocked this side
						blocked_count += 1
						break
					elif space[y + 1][now_x] <= 0:
						# create a new water head
						if (y, now_x) not in seen:
							space[y][now_x] = -1
							new_water_heads.append([y, now_x])
							seen.add((y, now_x))
						break
					space[y][now_x] = -1
				now_x = x
				while space[y][now_x] <= 0 and space[y + 1][now_x] >= 1:
					# We can't move down: we need to go sideways
					now_x -= 1
					if space[y][now_x] >= 1:
						# blocked this side
						if blocked_count == 1:
							anything_filled = True
							curr_x = now_x + 1
							# fill row in
							while space[y][curr_x] <= 0:
								space[y][curr_x] = 2
								curr_x += 1
						break
					elif space[y + 1][now_x] <= 0:
						# create a new water head
						space[y][now_x] = -1
						if (y, now_x) not in seen:
							new_water_heads.append([y, now_x])
							seen.add((y, now_x))
						break
					space[y][now_x] = -1
			
			water_heads = new_water_heads
	o = 0
	for y, line in enumerate(space):
		s = ""
		for x, pos in enumerate(line):
			if pos in (-1, 2) and y >= max_pos[2]:
				o += 1
			if pos == -1:
				s += "|"
			elif pos == 0:
				s += "."
			elif pos == 1:
				s += "#"
			elif pos == 2:
				s += "~"
		#print(s)
	return o

	# Below 33728
	# Above 33713

def part2(inp):
	max_pos = [0, 500, math.inf]
	vein_positions = set()
	for vein in inp:
		for y, x in zip(vein[0], vein[1]):
			max_pos = [max(y, max_pos[0]), max(x, max_pos[1]), min(y, max_pos[2])]
			vein_positions.add((y, x))
	space = []
	for y in range(max_pos[0] + 1):
		row = []
		for x in range(max_pos[1] + 1):
			if (y, x) in vein_positions:
				row.append(1)
			else:
				row.append(0)
		row.append(0)
		space.append(row)
	water_source = (0, 500)
	# 0: empty
	# 1: clay
	# 2: water block
	# -1: water was here, but not solid
	anything_filled = True
	while anything_filled:
		anything_filled = False
		seen = set()
		# Send "pulses" of water to ~~deal 60 base power special water damage~~ flow until fully settling one row per split flow
		water_heads = [list(water_source)]
		while len(water_heads) > 0: # Tick for individual pulse
			new_water_heads = []
			for wh in water_heads:
				# tick each once
				y, x = wh
				# Move down if possible
				while y + 1 < len(space) and space[y + 1][x] <= 0:
					wh[0] += 1
					space[y + 1][x] = -1
					y += 1
				now_x = x
				if y + 1 >= len(space):
					continue
				blocked_count = 0
				while space[y][now_x] <= 0 and space[y + 1][now_x] >= 1:
					# We can't move down: we need to go sideways
					now_x += 1
					if space[y][now_x] >= 1:
						# blocked this side
						blocked_count += 1
						break
					elif space[y + 1][now_x] <= 0:
						# create a new water head
						if (y, now_x) not in seen:
							space[y][now_x] = -1
							new_water_heads.append([y, now_x])
							seen.add((y, now_x))
						break
					space[y][now_x] = -1
				now_x = x
				while space[y][now_x] <= 0 and space[y + 1][now_x] >= 1:
					# We can't move down: we need to go sideways
					now_x -= 1
					if space[y][now_x] >= 1:
						# blocked this side
						if blocked_count == 1:
							anything_filled = True
							curr_x = now_x + 1
							# fill row in
							while space[y][curr_x] <= 0:
								space[y][curr_x] = 2
								curr_x += 1
						break
					elif space[y + 1][now_x] <= 0:
						# create a new water head
						space[y][now_x] = -1
						if (y, now_x) not in seen:
							new_water_heads.append([y, now_x])
							seen.add((y, now_x))
						break
					space[y][now_x] = -1
			
			water_heads = new_water_heads
	o = 0
	for y, line in enumerate(space):
		s = ""
		for x, pos in enumerate(line):
			if pos == 2 and y >= max_pos[2]:
				o += 1
			if pos == -1:
				s += "|"
			elif pos == 0:
				s += "."
			elif pos == 1:
				s += "#"
			elif pos == 2:
				s += "~"
		#print(s)
	return o

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
