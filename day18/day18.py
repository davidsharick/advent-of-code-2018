#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """.#.#...|#.
.....#|##|
.|..|...#.
..|#.....#
#.#|||#|#|
...#.||...
.|....|...
||...#|.#|
|.||||..|.
...#.|..|."""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 1147)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 0)
		pass

def parse_input(inp, p2mode = False):
	return readin.grid_input(inp, mapping = {".": 0, "|": 1, "#": 2})
	#return readin.int_list_input(inp)
	#return readin.chars_input(inp)

def part1(inp):
	area = inp
	ranges = ((0, len(inp) - 1), (0, len(inp[0]) - 1))
	for t in range(10):
		new_area = []
		for y in range(len(area)):
			new_row = []
			row = area[y]
			for x in range(len(row)):
				curr = row[x]
				ns = grid.get_neighbors(2, (y, x), ranges)
				if curr == 0:
					if sum(area[y][x] == 1 for y, x in ns) >= 3:
						new_row.append(1)
					else:
						new_row.append(0)
				elif curr == 1:
					if sum(area[y][x] == 2 for y, x in ns) >= 3:
						new_row.append(2)
					else:
						new_row.append(1)
				elif curr == 2:
					if any(area[y][x] == 1 for y, x in ns) and any(area[y][x] == 2 for y, x in ns):
						new_row.append(2)
					else:
						new_row.append(0)

		
			new_area.append(new_row)
		area = new_area
		#print(area)
	num_wooded = 0
	num_lumber = 0
	for row in area:
		num_wooded += sum(c == 1 for c in row)
		num_lumber += sum(c == 2 for c in row)
	return num_wooded * num_lumber

def part2(inp):
	area = inp
	ranges = ((0, len(inp) - 1), (0, len(inp[0]) - 1))
	seens = [[], []]
	seen_states = set()
	state_times = {}
	for t in range(10000):
		new_area = []
		for y in range(len(area)):
			new_row = []
			row = area[y]
			for x in range(len(row)):
				curr = row[x]
				ns = grid.get_neighbors(2, (y, x), ranges)
				if curr == 0:
					if sum(area[y][x] == 1 for y, x in ns) >= 3:
						new_row.append(1)
					else:
						new_row.append(0)
				elif curr == 1:
					if sum(area[y][x] == 2 for y, x in ns) >= 3:
						new_row.append(2)
					else:
						new_row.append(1)
				elif curr == 2:
					if any(area[y][x] == 1 for y, x in ns) and any(area[y][x] == 2 for y, x in ns):
						new_row.append(2)
					else:
						new_row.append(0)

		
			new_area.append(new_row)
		area = new_area

		s = ""
		for row in area:
			s += "".join(str(c) for c in row)
			s += "\n"
		if s in seen_states:
			cycle_start = state_times[s]
			cycle_end = t - 1
			cycle_length = t - cycle_start
			time_in_cycle = 1000000000 - cycle_start - 1
			extra_time = time_in_cycle % cycle_length
			need_time = cycle_start + extra_time
			for state, time in state_times.items():
				if time == need_time:
					return sum(c == "1" for c in state) * sum(c == "2" for c in state)
			assert False
		seen_states.add(s)
		state_times[s] = t
	
	return 0

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
