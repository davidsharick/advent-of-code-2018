#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 0)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 0)
		pass

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		if idx == 0:
			ip_reg = int(line.strip().split()[1])
		else:
			l = []
			parts = line.strip().split()
			l.append(parts[0])
			for part in parts[1:]:
				l.append(int(part))
			o.append(l)
	return o, ip_reg
	#return readin.int_list_input(inp)
	#return readin.chars_input(inp)

def addr(args, registers):
	return registers[args[0]] + registers[args[1]]

def addi(args, registers):
	return registers[args[0]] + args[1]

def mulr(args, registers):
	return registers[args[0]] * registers[args[1]]

def muli(args, registers):
	return registers[args[0]] * args[1]

def banr(args, registers):
	return registers[args[0]] & registers[args[1]]

def bani(args, registers):
	return registers[args[0]] & args[1]

def borr(args, registers):
	return registers[args[0]] | registers[args[1]]

def bori(args, registers):
	return registers[args[0]] | args[1]

def setr(args, registers):
	return registers[args[0]]

def seti(args, registers):
	return args[0]

def gtri(args, registers):
	return 1 if registers[args[0]] > args[1] else 0

def gtir(args, registers):
	return 1 if args[0] > registers[args[1]] else 0

def gtrr(args, registers):
	return 1 if registers[args[0]] > registers[args[1]] else 0

def eqri(args, registers):
	return 1 if registers[args[0]] == args[1] else 0

def eqir(args, registers):
	return 1 if args[0] == registers[args[1]] else 0

def eqrr(args, registers):
	return 1 if registers[args[0]] == registers[args[1]] else 0

# to avoid eval() (evil)
mapping = {"addr": addr, "addi": addi, "mulr": mulr, "muli": muli, "setr": setr, "seti": seti, "gtri": gtri, "gtir": gtir, "gtrr": gtrr, "eqri": eqri, "eqir": eqir, "eqrr": eqrr}

def part1(inp):
	r1 = 1
	r5 = 864
	r0 = 0
	"""pairs = []
	for i1 in range(1, int(math.sqrt(r5)) + 1):
		if r5 % i1 == 0:
			pairs.append((i1, r5 // i1))
	pr(pairs)
	pr(sum(p[0] for p in pairs) + sum(p[1] for p in pairs))"""
	while True:
		r3 = 1
		while True:
			r2 = r1 * r3
			if r2 == r5:
				r0 += r1
			r3 += 1
			if r3 > r5:
				break
		r1 += 1
		if r1 > r5:
			break
	return r0
	"""prog, ip_reg = inp
	registers = [0, 0, 0, 0, 0, 0]
	ip = 0
	while True:
		if ip < 0 or ip >= len(prog):
			break
		if ip == 2:
			pr(registers)
			return
		registers[ip_reg] = ip
		instr = prog[ip]
		registers[instr[3]] = mapping[instr[0]](instr[1:], registers)
		ip = registers[ip_reg]
		ip += 1
	return registers[0]"""

def part2(inp):
	prog, ip_reg = inp
	registers = [1, 0, 0, 0, 0, 0]
	ip = 0
	# Find r[5]
	while True:
		if ip < 0 or ip >= len(prog):
			break
		if ip == 1:
			break
		registers[ip_reg] = ip
		instr = prog[ip]
		registers[instr[3]] = mapping[instr[0]](instr[1:], registers)
		ip = registers[ip_reg]
		ip += 1
	
	r5 = registers[5]
	pairs = []
	for i1 in range(1, int(math.sqrt(r5)) + 1):
		if r5 % i1 == 0:
			pairs.append((i1, r5 // i1))
	return sum(p[0] for p in pairs) + sum(p[1] for p in pairs)
	"""while True:
		r3 = 1
		while True:
			r2 = r1 * r3
			if r2 == r5:
				r0 += r1
			r3 += 1
			if r3 > r5:
				#pr(r0, r1, r2, r3, r5)
				break
		r1 += 1
		if r1 > r5:
			break
	return r0"""

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
