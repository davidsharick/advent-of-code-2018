#ip 4
addi 4 16 4     # Increment IP by 16, going to (addi 5 2 5)?
seti 1 1 1      # r[1] = 1
seti 1 7 3      # r[3] = 1
mulr 1 3 2      # r[2] = r[1] * r[3], so if prev 2 were executed, always 1; otherwise, we win iff at least one of them is zero and r[5] is zero
eqrr 2 5 2      # r[2] = (r[2] == r[5])
addr 2 4 4      # r[4] += r[2], so jump over next instr if r[2] == r[5]
addi 4 1 4      # r[4] += 1, so jump over next instr (unless we jumped last instr)
addr 1 0 0      # r[0] += r[1]
addi 3 1 3      # r[3] += 1
gtrr 3 5 2      # r[2] = (r[3] > r[5])
addr 4 2 4      # r[4] += r[2], so jump over next instr if r[3] > r[5]
seti 2 3 4      # r[4] = 2, so go to (mulr 1 3 2) if we didn't jump last (note the values of r[1]/r[3] may vary here)
addi 1 1 1      # r[1] += 1
gtrr 1 5 2      # r[2] = (r[1] > r[5])
addr 2 4 4      # r[4] += r[2], so jump over next instr if r[1] > r[5]
seti 1 6 4      # r[4] = 1, so jump to (seti 1 7 3) if we didn't jump last;
mulr 4 4 4      # r[4] *= r[4]; this will take the IP out of the valid range, ending the execution
addi 5 2 5      # r[5] += r[2]; to my understanding, this and further is only run once?
mulr 5 5 5      # r[5] *= r[5]
mulr 4 5 5      # r[5] *= r[4]
muli 5 11 5     # r[5] *= 11
addi 2 1 2      # r[2] += 1
mulr 2 4 2      # r[2] *= r[4]
addi 2 6 2      # r[2] += 6
addr 5 2 5      # r[5] += r[2]
addr 4 0 4      # r[4] += r[0]; jump by value in r[0]; if r[0] == 0 and r[5] == 1 here we win; in star 1 this does nothing, in star 2 this leads instrs 27: to run while skipping 26
seti 0 0 4      # r[4] = 0; goes to start of part 1, meaning if r[5] is set right, we win; from (seti 1 1 1) it has to be exactly 1 to win
setr 4 5 2      # r[2] = r[4]
mulr 2 4 2      # r[2] *= r[4]
addr 4 2 2      # r[2] += r[4]
mulr 4 2 2      # r[2] *= r[4]
muli 2 14 2     # r[2] *= 14
mulr 2 4 2      # r[2] *= r[4]
addr 5 2 5      # r[5] += r[2]
seti 0 5 0      # r[0] = 0
seti 0 2 4      # r[4] = 0, which goes to (seti 1 1 1)


# r[5] will be 10551264 when reaching (seti 1 1 1) for the first time


r1 = 1
r5 = 10550400
r0 = 0
while True:
    r3 = 1
    while True:
        r2 = r1 * r3
        if r2 == r5:
            r0 += r1
        r3 += 1
        if r3 > r5:
            break
    r1 += 1
    if r1 > r5:
        break
return r0

HOW TO ESCAPE THE MATRIX:

- execute (mulr 4 4 4)
-> execute (addr 2 4 4) with r[2] == 1
-> execute (gtrr 1 5 2) with r[1] > r[5]
-> 