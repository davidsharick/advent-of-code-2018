#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """abcdef
bababc
abbcde
abcccd
aabcdd
abcdee
ababab"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = """abcde
fghij
klmno
pqrst
fguij
axcye
wvxyz"""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 12)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), "fgij")
		pass

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		o.append(line.strip())
	return o

def part1(inp):
	num_2s = 0
	num_3s = 0
	for line in inp:
		c = Counter(line)
		for k, v in c.items():
			if v == 2:
				num_2s += 1
				break
		for k, v in c.items():
			if v == 3:
				num_3s += 1
				break
	return num_2s * num_3s

def part2(inp):
	for line in inp:
		for line2 in inp:
			if line == line2:
				continue
			diffs = 0
			for idx, c in enumerate(line):
				if c != line2[idx]:
					diffs += 1
				if diffs > 1:
					break
			if diffs == 1:
				o = ""
				for idx, c in enumerate(line):
					if c == line2[idx]:
						o += c
				return o

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
