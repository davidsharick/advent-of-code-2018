#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.direction as direc
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 31)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 0)
		pass


dir_dict = {"N": direc.DOWN, "S": direc.UP, "E": direc.RIGHT, "W": direc.LEFT}

class Node:
	def __init__(self, data):
		self.node_data = []
		seeing_children = False
		while True:
			next_value = data.pop(0)
			if next_value in "NEWS":
				self.node_data.append(next_value)
			elif next_value == "(":
				self.node_data.append([])
				self.node_data[-1].append(Node(data))
				seeing_children = True
			elif next_value == "|":
				if seeing_children:
					if type(self.node_data[-1]) is list:
						self.node_data[-1].append(Node(data))
					else:
						pr(self.node_data)
						assert False
				else:
					data.insert(0, "|")
					return
			elif next_value == ")":
				if seeing_children:
					seeing_children = False
				else:
					data.insert(0, ")")
					return
			elif next_value == "$":
				return
	
	def expand_graph(self, nodes, edges, pos):
		y, x = pos
		nodes.add(pos)
		for value in self.node_data:
			prev_pos = (y, x)
			if type(value) is str:
				direc = dir_dict[value]
				y += direc[0]
				x += direc[1]
				pos = (y, x)
				nodes.add(pos)
				edges[pos].add(prev_pos)
				edges[prev_pos].add(pos)
			else:
				curr_pos = (y, x)
				for child in value:
					child.expand_graph(nodes, edges, curr_pos)
	

def parse_input(inp, p2mode = False):
	for idx, line in enumerate(inp):
		return Node(list(line.strip()))
	#return readin.int_list_input(inp)
	#return readin.chars_input(inp)

def part1(inp):
	"""pr(inp.node_data)
	for node in inp.node_data:
		if type(node) is list:
			for node2 in node:
				pr(node2.node_data)"""
	graph_nodes = set()
	graph_edges = defaultdict(set)
	inp.expand_graph(graph_nodes, graph_edges, (0, 0))
	for k, v in graph_edges.items():
		graph_edges[k] = list(graph.Edge(dest, 1) for dest in v)
	dist, prev = graph.Graph(graph_nodes, graph_edges).dijkstra((0, 0))
	return max(d for d in dist.values()), dist

def part2(inp):
	return sum(v >= 1000 for v in inp.values())
	return 0

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	ans, dist = part1(i)
	print(ans)
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(dist))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
