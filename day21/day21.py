#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 0)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 0)
		pass

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		if idx == 0:
			ip_reg = int(line.strip().split()[1])
		else:
			l = []
			parts = line.strip().split()
			l.append(parts[0])
			for part in parts[1:]:
				l.append(int(part))
			o.append(l)
	return o, ip_reg
	#return readin.int_list_input(inp)
	#return readin.chars_input(inp)

def addr(args, registers):
	return registers[args[0]] + registers[args[1]]

def addi(args, registers):
	return registers[args[0]] + args[1]

def mulr(args, registers):
	return registers[args[0]] * registers[args[1]]

def muli(args, registers):
	return registers[args[0]] * args[1]

def banr(args, registers):
	return registers[args[0]] & registers[args[1]]

def bani(args, registers):
	return registers[args[0]] & args[1]

def borr(args, registers):
	return registers[args[0]] | registers[args[1]]

def bori(args, registers):
	return registers[args[0]] | args[1]

def setr(args, registers):
	return registers[args[0]]

def seti(args, registers):
	return args[0]

def gtri(args, registers):
	return 1 if registers[args[0]] > args[1] else 0

def gtir(args, registers):
	return 1 if args[0] > registers[args[1]] else 0

def gtrr(args, registers):
	return 1 if registers[args[0]] > registers[args[1]] else 0

def eqri(args, registers):
	return 1 if registers[args[0]] == args[1] else 0

def eqir(args, registers):
	return 1 if args[0] == registers[args[1]] else 0

def eqrr(args, registers):
	return 1 if registers[args[0]] == registers[args[1]] else 0

# to avoid eval() (evil)
mapping = {"addr": addr, "addi": addi, "mulr": mulr, "muli": muli, "banr": banr, "bani": bani, "borr": borr, "bori": bori, "setr": setr, "seti": seti, "gtri": gtri, "gtir": gtir, "gtrr": gtrr, "eqri": eqri, "eqir": eqir, "eqrr": eqrr}

def part1(inp):
	prog, ip_reg = inp
	registers = [7216956, 0, 0, 0, 0, 0]
	ip = 0
	ips = []
	while True:
		if ip < 0 or ip >= len(prog):
			pr(registers)
			break
		registers[ip_reg] = ip
		instr = prog[ip]
		ips.append(ip)
		if instr[0] == "eqrr":
			#for l in (l for l in itertools.groupby(ips, lambda q: q == 25)):
			#	pr(list(l[1]))
			return registers[3]
		registers[instr[3]] = mapping[instr[0]](instr[1:], registers)
		ip = registers[ip_reg]
		ip += 1
	return registers[0]

def code():
	r3 = 0
	x = []
	r0 = 0 # to find
	r4 = 0
	r5 = 0
	while True:
		r2 = r3 | 65536
		r3 = 1505483
		while True:
			r3 += (r2 & 255)
			r3 &= 0xFFFFFF
			r3 *= 65899
			r3 &= 0xFFFFFF
			if r2 < 256:
				break
			else:
				r4 = 0
				while True:
					r5 = r4 + 1
					r5 *= 256
					if r5 > r2:
						break
					else:
						r4 += 1
				r2 = r4
		if r3 == r0:
			break
		else:
			if r3 in x:
				return x[-1]
			x.append(r3)
# Above 9638656
def part2(inp):
	return code()


def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
