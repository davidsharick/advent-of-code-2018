#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """depth: 510
target: 10,10"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 114)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 45)
		pass

def parse_input(inp, p2mode = False):
	for idx, line in enumerate(inp):
		if idx == 0:
			depth = int(line.strip().split()[1])
		elif idx == 1:
			target_coords = list(int(c) for c in reversed(line.strip().split()[1].split(",")))
	return depth, target_coords
	#return readin.int_list_input(inp)
	#return readin.chars_input(inp)

def part1(inp):
	depth, target = inp
	ym, xm = target
	area = []
	for y in range(ym + 1):
		row = []
		for x in range(xm + 1):
			row.append(-1)
		area.append(row)
	area[0][0] = 0
	area[ym][xm] = 0
	running_total = 0
	for y, row in enumerate(area):
		#s = ""
		for x in range(len(row)):
			if (y == ym and x == xm) or (y == 0 and x == 0):
				continue
			if y == 0 and x != 0:
				row[x] = 16807 * x
			elif x == 0 and y != 0:
				row[x] = y * 48271
			else:
				above_geo = area[y - 1][x]
				left_geo = area[y][x - 1]
				above_el = (above_geo + depth) % 20183
				left_el = (left_geo + depth) % 20183
				row[x] = above_el * left_el
			curr_el = (row[x] + depth) % 20183
			running_total += (curr_el % 3)
			#s += ({0: ".", 1: "=", 2: "|"}[curr_el % 3])
		#print(s)
	return running_total

def part2(inp):
	depth, target = inp
	ym, xm = target
	area = []
	for y in range(ym + 25): # hacky
		row = []
		for x in range(xm + 25):
			row.append(-1)
		area.append(row)
	area[0][0] = 0
	area[ym][xm] = 0
	# fill in values
	for y, row in enumerate(area):
		for x in range(len(row)):
			if (y == ym and x == xm) or (y == 0 and x == 0):
				continue
			if y == 0 and x != 0:
				row[x] = 16807 * x
			elif x == 0 and y != 0:
				row[x] = y * 48271
			else:
				above_geo = area[y - 1][x]
				left_geo = area[y][x - 1]
				above_el = (above_geo + depth) % 20183
				left_el = (left_geo + depth) % 20183
				row[x] = above_el * left_el
	
	# change to risk levels
	for y, row in enumerate(area):
		area[y] = list(map(lambda p: ((p + depth) % 20183) % 3, row))
	area[0][0] = 0
	area[ym][xm] = 0
	nodes = []
	edges = defaultdict(list)
	# 0 = neither
	# 1 = torch
	# 2 = climbing
	ranges = ((0, len(area) - 1), (0, len(area[0]) - 1))
	target = (ym, xm, 1)
	for y in range(len(area)):
		for x in range(len(area[y])):
			curr_terrain = area[y][x]
			if curr_terrain == 0: # rocky, no neither
				nodes.append((y, x, 1))
				nodes.append((y, x, 2))
				curr_options = (1, 2)
			elif curr_terrain == 1: # wet, no torch
				nodes.append((y, x, 0))
				nodes.append((y, x, 2))
				curr_options = (0, 2)
			elif curr_terrain == 2: # narrow, no climbing gear
				nodes.append((y, x, 0))
				nodes.append((y, x, 1))
				curr_options = (0, 1)
			for opt in curr_options:
				# fill in edges
				curr_node = (y, x, opt)
				neighbors = grid.get_neighbors(2, (y, x), ranges, corners = False)
				# add the neighbors that are within allowed gear
				for neighbor in neighbors:
					ny, nx = neighbor
					neighboring_rl = area[ny][nx]
					if allowed_passage(neighboring_rl, opt):
						# we can use the current opt to transit to the neighbor
						edges[curr_node].append(graph.Edge((ny, nx, opt), 1))
			# add the last edges
			edges[(y, x, curr_options[0])].append(graph.Edge((y, x, curr_options[1]), 7))
			edges[(y, x, curr_options[1])].append(graph.Edge((y, x, curr_options[0]), 7))
	#pr(nodes, edges)
	dist, prev = graph.Graph(nodes, edges).dijkstra((0, 0, 1))
	#pr(dist[target])
	curr = target
	#while curr != (0, 0, 1):
	#	pr(curr, dist[curr])
	#	curr = prev[curr]
			
	return dist[target]

def allowed_passage(risk_level, equipment):
	if risk_level == 0:
		return equipment in (1, 2)
	elif risk_level == 1:
		return equipment in (0, 2)
	elif risk_level == 2:
		return equipment in (0, 1)
# below 1040
def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
