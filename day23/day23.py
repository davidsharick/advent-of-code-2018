#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """pos=<0,0,0>, r=4
pos=<1,0,0>, r=1
pos=<4,0,0>, r=3
pos=<0,2,0>, r=1
pos=<0,5,0>, r=3
pos=<0,0,3>, r=1
pos=<1,1,1>, r=1
pos=<1,1,2>, r=1
pos=<1,3,1>, r=1"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = """pos=<10,12,12>, r=2
pos=<12,14,12>, r=2
pos=<16,12,12>, r=4
pos=<14,14,14>, r=6
pos=<50,50,50>, r=200
pos=<10,10,10>, r=5"""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 7)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 35)
		pass
	
	def test_cube_intersection(self):
		self.assertEqual(cube_nanobot_intersect(((1, 0, -6213538), (2, 1, -6213537)), [-22356506, 24819383, 19709017], 53389427), False)

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		parts = line.strip().split(", ")
		r = int(parts[1].split("=")[1])
		pos = list(int(c) for c in parts[0].split("<")[1][:-1].split(","))
		o.append((pos, r))
	return o
	#return readin.int_list_input(inp)
	#return readin.chars_input(inp)

def part1(inp):
	best = ((0, 0, 0), 0)
	for nanobot in inp:
		pos, rad = nanobot
		if rad > best[1]:
			best = (pos, rad)
	found = 0
	for nanobot in inp:
		manh = sum(abs(best[0][dim] - nanobot[0][dim]) for dim in range(3))
		if manh <= best[1]:
			found += 1
	return found

def part2(inp):
	# approach "borrowed" from https://old.reddit.com/r/adventofcode/comments/aa9uvg/day_23_aoc_creators_logic/ecrftas/
	largest_dist = 0
	for pos, rad in inp:
		manh = sum(abs(pos[dim]) for dim in range(3))
		largest_dist = max(manh + rad, largest_dist)
	# partition space into 8 cubes with (0, 0, 0) as mutual corner
	cubes = partition_space(((-largest_dist, -largest_dist, -largest_dist), (largest_dist, largest_dist, largest_dist)), (0, 0, 0))
	cont = True
	while cont:
		cube_vals = [0, 0, 0, 0, 0, 0, 0, 0]
		for i, cube in enumerate(cubes):
			for nanobot in inp:
				if cube_nanobot_intersect(cube, nanobot[0], nanobot[1]):
					#pr(f"Cube {cube} in range of nanobot {nanobot}")
					cube_vals[i] += 1
				else:
					pass#pr(f"Cube {cube} out of range of nanobot {nanobot}")
		best_val = max(cube_vals)
		for i in range(8):
			if cube_vals[i] == best_val:
				new_center, cont = cube_center(cubes[i])
				if not cont:
					final_cube = cubes[i]
					break
				cubes = partition_space(cubes[i], new_center)
				break
	return best_coord(final_cube, inp)
	return 0

def best_coord(cube, nanobots):
	cube_values = Counter()
	for cube_id in range(8):
		x = cube[cube_id & 1][0]
		y = cube[(cube_id & 2) // 2][1]
		z = cube[(cube_id & 4) // 4][2]
		cell = (x, y, z)
		for pos, rad in nanobots:
			manh = sum(abs(cell[dim] - pos[dim]) for dim in range(3))
			if manh <= rad:
				#pr(f"Position {cell} in range of nanobot {pos}, {rad}; dist {manh}")
				cube_values[cube_id] += 1
			else:
				pass#pr(f"Position {cell} out of range of nanobot {pos}, {rad}; dist {manh}")
	best_id = max(cube_values.items(), key = lambda k: k[1])[0]
	x = cube[best_id & 1][0]
	y = cube[(best_id & 2) // 2][1]
	z = cube[(best_id & 4) // 4][2]
	best_pos = (x, y, z)
	return sum(abs(p) for p in best_pos)

def cube_center(cube):
	center_posn = (
		cube[0][0] + ((cube[1][0] - cube[0][0]) // 2),
		cube[0][1] + ((cube[1][1] - cube[0][1]) // 2),
		cube[0][2] + ((cube[1][2] - cube[0][2]) // 2)
	)
	if (cube[1][0] - cube[0][0]) < 2 and (cube[1][1] - cube[0][1]) < 2 and (cube[1][2] - cube[0][2]) < 2:
		return center_posn, False
	return center_posn, True

def partition_space(outer_cube, center_posn):
	outer_lower, outer_upper = outer_cube
	cubes = []
	# Everything is either: (lower -> center) or (center -> upper)
	# We can define bit == 0 as former, bit == 1 as latter
	for cube_id in range(8):
		x_pick = cube_id & 1
		y_pick = (cube_id & 2) // 2
		z_pick = (cube_id & 4) // 4
		cube = [[], []]
		if x_pick:
			cube[0].append(center_posn[0])
			cube[1].append(outer_upper[0])
		else:
			cube[0].append(outer_lower[0])
			cube[1].append(center_posn[0])

		if y_pick:
			cube[0].append(center_posn[1])
			cube[1].append(outer_upper[1])
		else:
			cube[0].append(outer_lower[1])
			cube[1].append(center_posn[1])

		if z_pick:
			cube[0].append(center_posn[2])
			cube[1].append(outer_upper[2])
		else:
			cube[0].append(outer_lower[2])
			cube[1].append(center_posn[2])
		
		cube = (tuple(cube[0]), tuple(cube[1]))
		cubes.append(cube)
	return cubes

def cube_nanobot_intersect(cube, pos, rad):
	# validate cube
	assert cube[0][0] <= cube[1][0]
	assert cube[0][1] <= cube[1][1]
	assert cube[0][2] <= cube[1][2]

	cube_lower, cube_upper = cube
	# check: 
	# all 6 faces
	# entirely within

	# mod 3: which idx to vary (other 2 are always 0, 0 and 1, 1)
	# floor 3: whether other is 0 or 1

	# top face: ((cl[0], cu[1], cl[2]), (cu[0], cu[1], cu[2]))

	faces = []

	# if the center is within the cube, true by default; otherwise, we have to check if it intercepts any faces
	if all(cube[0][dim] <= pos[dim] <= cube[1][dim] for dim in range(3)):
		#pr("True by center presence")
		return True

	for face_num in range(6):
		vary_idx = face_num % 3
		vary_val = face_num // 3

		ref_pos = cube_upper if vary_val else cube_lower
		
		if vary_idx == 0:
			# vary_idx encoded for ease of access later
			face = ((ref_pos[0], cube_lower[1], cube_lower[2]), (ref_pos[0], cube_upper[1], cube_upper[2]), vary_idx)
		elif vary_idx == 1:
			face = ((cube_lower[0], ref_pos[1], cube_lower[2]), (cube_upper[0], ref_pos[1], cube_upper[2]), vary_idx)
		elif vary_idx == 2:
			face = ((cube_lower[0], cube_lower[1], ref_pos[2]), (cube_upper[0], cube_upper[1], ref_pos[2]), vary_idx)
		
		faces.append(face)
	
	for face in faces:
		if face_nanobot_intersect(face, pos, rad):
			#pr(f"True by face {face}")
			return True
	
	#pr("False")
	return False


def face_nanobot_intersect(face, pos, rad):
	#pr(face, pos, rad)
	face_lower, face_upper, plane_idx = face
	assert face_lower[plane_idx] == face_upper[plane_idx]
	plane_coord = face_lower[plane_idx]
	dist = abs(pos[plane_idx] - plane_coord)
	plane_center = []
	plane_face = [[], []]
	for dimension in range(3):
		if dimension == plane_idx:
			pass
		else:
			plane_center.append(pos[dimension])
			plane_face[0].append(face_lower[dimension])
			plane_face[1].append(face_upper[dimension])
	remaining_rad = rad - dist
	if remaining_rad <= 0:
		return False
	# Problem has now been reduced to 2D with face_nanobot_intersect(plane_face, plane_center, remaining_rad)
	plane_lower, plane_upper = plane_face
	#pr(f"Center {plane_center}, distance {remaining_rad}, face {plane_face}")

	# Check if the 2D projection's center is within the face; this covers the "entirely within" case
	if (plane_lower[0] <= plane_center[0] <= plane_upper[0]) and (plane_lower[1] <= plane_center[1] <= plane_upper[1]):
		return True

	edges = [
		((plane_lower[0], plane_lower[1]), (plane_lower[0], plane_upper[1])),
		((plane_lower[0], plane_lower[1]), (plane_upper[0], plane_lower[1])),
		((plane_lower[0], plane_upper[1]), (plane_upper[0], plane_upper[1])),
		((plane_upper[0], plane_lower[1]), (plane_upper[0], plane_upper[1])),
	]
	#pr(edges)
	for edge in edges:
		if edge[0][0] == edge[1][0]:
			# x (we'll call it x) is constant, so find the x-range
			relative_center = plane_center[1]
			remaining_dist = remaining_rad - abs(plane_center[0] - edge[0][0])
			#pr(f"Relative center is {relative_center}; remaining {remaining_dist}")
			if remaining_dist <= 0:
				continue
			else:
				lower_bound = relative_center - remaining_dist
				upper_bound = relative_center + remaining_dist
				if (edge[0][1] <= lower_bound <= edge[1][1]) or (edge[0][1] <= upper_bound <= edge[1][1]):
					#pr(f"True by edge {edge}; {lower_bound} to {upper_bound}")
					return True
				elif (lower_bound <= edge[0][1]) and (upper_bound >= edge[1][1]):
					return True
		else:
			relative_center = plane_center[0]
			remaining_dist = remaining_rad - abs(plane_center[1] - edge[0][1])
			if remaining_dist <= 0:
				continue
			else:
				lower_bound = relative_center - remaining_dist
				upper_bound = relative_center + remaining_dist
				if (edge[0][0] <= lower_bound <= edge[1][0]) or (edge[0][0] <= upper_bound <= edge[1][0]):
					return True
				elif (lower_bound <= edge[0][0]) and (upper_bound >= edge[1][0]):
					return True
	
	return False

# Failed attempt that was bad
"""def all_positions(pos, rad):
	o = []
	#shell_size = 2 + (4 * rad) - 4
	shell_size = 0
	for width in range(1, rad):
		shell_size += (4 * (width + 1)) - 4
		#pr(shell_size, rad, width)
	shell_size *= 2
	shell_size += (2 + (4 * rad) - 4)
	pr(pos, rad, shell_size)
	return
	pr(pos, rad)
	for z in range(-rad, rad + 1):
		max_other = abs(rad - abs(z))
		for y in range(-max_other, max_other + 1):
			max_x = abs(max_other - abs(y))
			for x in (-max_x, max_x):
				#if abs(x) + abs(y) + abs(z) == rad:
				o.append((x + pos[0], y + pos[1], z + pos[2]))
	pr(len(o))
	#assert all(sum(abs(c) for c in pos) == rad for pos in o)
	return o"""

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
