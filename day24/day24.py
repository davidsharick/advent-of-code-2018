#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """Immune System:
17 units each with 5390 hit points (weak to radiation, bludgeoning) with an attack that does 4507 fire damage at initiative 2
989 units each with 1274 hit points (immune to fire; weak to bludgeoning, slashing) with an attack that does 25 slashing damage at initiative 3

Infection:
801 units each with 4706 hit points (weak to radiation) with an attack that does 116 bludgeoning damage at initiative 1
4485 units each with 2961 hit points (immune to radiation; weak to fire, cold) with an attack that does 12 slashing damage at initiative 4"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 5216)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 51)
		pass

class Unit():
	def __init__(self, count, hp, immunities, weaknesses, attack, attack_type, speed):
		self.count = count
		self.hp = hp
		self.immunities = immunities
		self.weaknesses = weaknesses
		self.attack = attack
		self.attack_type = attack_type
		self.speed = speed
		self.is_infection = False
	
	# https://stackoverflow.com/questions/141545/how-to-overload-init-method-based-on-argument-type
	@classmethod
	def from_line(cls, line):
		parts = list(line.split())
		units = int(parts[0])
		hp = int(parts[4])
		speed = int(parts[-1])
		in_immunities = False
		in_weaknesses = False
		type_matchups = parts[7:-11]
		weak = []
		immune = []
		for idx, part in enumerate(type_matchups):
			if part.endswith("immune"):
				in_immunities = True
				in_weaknesses = False
			elif part.endswith("weak"):
				in_immunities = False
				in_weaknesses = True
			elif part == "to":
				continue
			else:
				if in_immunities:
					immune.append(part.strip(",").strip(")").strip(";"))
				elif in_weaknesses:
					weak.append(part.strip(",").strip(")").strip(";"))
		attack_type = parts[-5]
		attack = int(parts[-6])

		return cls(units, hp, immune, weak, attack, attack_type, speed)

		"""for idx, part in enumerate(parts):
			if part.startswith("("):
				if part.endswith("immune"):
					in_immunities = True
				elif part.endswith("weak"):
					in_weaknesses = True
			elif in_immunities:"""
	
	def __repr__(self):
		return f"Unit with {self.count} units; {self.attack} attack of type {self.attack_type}; {self.hp} HP; {self.speed} speed; weak to {self.weaknesses} and immune to {self.immunities}; {self.is_infection}"
	
	def uid(self):
		return f"{self.attack}; {self.attack_type}; {self.hp}; {self.speed}; {self.weaknesses}; {self.immunities}"
	
	def copy(self):
		u = Unit(self.count, self.hp, self.immunities, self.weaknesses, self.attack, self.attack_type, self.speed)
		u.is_infection = self.is_infection
		return u


def parse_input(inp, p2mode = False):
	immune = []
	infection = []
	in_immune = False
	in_infection = False
	for idx, line in enumerate(inp):
		if "Immune System" in line:
			in_immune = True
		elif "Infection" in line:
			in_immune = False
			in_infection = True
		elif in_immune and len(line.strip()) > 0:
			unit = Unit.from_line(line.strip())
			unit.is_infection = False
			immune.append(unit)
		elif in_infection and len(line.strip()) > 0:
			unit = Unit.from_line(line.strip())
			unit.is_infection = True
			infection.append(unit)
	return immune, infection
	#return readin.int_list_input(inp)
	#return readin.chars_input(inp)

def part1(inp):
	end_units = combat_sim(inp)
	return sum(u.count for u in end_units)

def combat_sim(units):
	immune_sys, infection = units
	full = immune_sys.copy()
	full.extend(infection)
	lens = set()
	while True:
		full.sort(key = lambda u: (u.count * u.attack) * 30 + u.speed, reverse = True)
		if len(tuple(filter(lambda u: u.is_infection, full))) <= 0 or len(tuple(filter(lambda u: not u.is_infection, full))) <= 0:
			break
		targets = {}
		unit_map = {}
		target_found = False
		for idx, unit in enumerate(full):
			best = ([], 0)
			damage = unit.count * unit.attack
			unit_map[unit.uid()] = idx
			for unit2 in full:
				if unit == unit2:
					continue
				if unit.is_infection == unit2.is_infection:
					continue
				if unit2 in targets.values():
					continue
				
				attack_power = damage
				if unit.attack_type in unit2.immunities:
					attack_power = 0
				elif unit.attack_type in unit2.weaknesses:
					attack_power *= 2
				
				if attack_power == best[1]:
					best[0].append(unit2)
				elif attack_power > best[1]:
					best = ([unit2], attack_power)
			best[0].sort(key = lambda u: (u.count * u.attack) * 30 + u.speed, reverse = True)
			if len(best[0]) > 0 and best[1] > 0:
				targets[idx] = best[0][0]
				target_found = True
			else:
				targets[idx] = None

		if not target_found: # stalemate:
			return None # should never happen ideally in part1
		full.sort(key = lambda u: u.speed, reverse = True)
		for unit in full:
			if unit.count <= 0:
				continue
			target = targets[unit_map[unit.uid()]]
			if target is None:
				continue
			damage = (unit.count * unit.attack)
			if unit.attack_type in target.weaknesses:
				damage *= 2
			elif unit.attack_type in target.immunities:
				damage = 0
			#pr(f"{unit} dealing {damage} damage to {target}, killing {min(target.count, (damage // target.hp))} units")
			target.count -= (damage // target.hp)
			target.count = max(0, target.count)
			full = list(filter(lambda u: u.count > 0, full))
	return full

	return 0

def part2(inp):
	min_boost = 0
	max_boost = 2 ** 16 # arbitrary
	known = {min_boost: False, max_boost: True}
	full = inp[0].copy()
	full.extend(inp[1])
	while True:
		target_boost = min_boost + ((max_boost - min_boost) // 2)
		if max_boost - min_boost == 1:
			target_boost = max_boost
		new_immune = []
		new_infec = []
		for unit in full:
			new = unit.copy()
			if not unit.is_infection:
				new.attack += target_boost
				new_immune.append(new)
			else:
				new_infec.append(new)
		
		end_units = combat_sim((new_immune, new_infec))
		if end_units is None:
			#pr(f"{target_boost} Win for infection by stalemate; {min_boost} to {max_boost}")
			known[target_boost] = True
			min_boost = target_boost
			continue
		if not end_units[0].is_infection:
			if (target_boost - 1) in known.keys() and known[target_boost - 1] == False:
				return sum(u.count for u in end_units)
			elif target_boost - 1 == min_boost:
				return sum(u.count for u in end_units)
			else:
				# lower half
				#pr(f"{target_boost} Win for immune system; {min_boost} to {max_boost}")
				known[target_boost] = True
				max_boost = target_boost

		else:
			# upper half
			#pr(f"{target_boost} Win for infection; {min_boost} to {max_boost}")
			known[target_boost] = True
			min_boost = target_boost
	return 0

# above 6971

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
