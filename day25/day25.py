#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """ 0,0,0,0
 3,0,0,0
 0,3,0,0
 0,0,3,0
 0,0,0,3
 0,0,0,6
 9,0,0,0
12,0,0,0"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = """1,-1,0,1
2,0,-1,0
3,2,-1,0
0,0,3,1
0,0,-1,-1
2,3,-2,0
-2,2,0,0
2,-2,0,-1
1,-1,0,-1
3,2,0,2"""
		self.test2 = parse_input(self.testinput2.split("\n"))

		self.testinput3 = """1,-1,-1,-2
-2,-2,0,1
0,2,1,3
-2,3,-2,1
0,2,3,-2
-1,-1,1,-2
0,-2,-1,0
-2,2,3,-1
1,2,2,0
-1,-2,0,-2"""
		self.test3 = parse_input(self.testinput3.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 2)
		self.assertEqual(part1(self.test2), 3)
		self.assertEqual(part1(self.test3), 8)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 0)
		pass

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		o.append(ast.literal_eval("(" + line.strip() + ")"))
	return o
	#return readin.int_list_input(inp)
	#return readin.chars_input(inp)

def part1(inp):
	nodes = inp
	edges = defaultdict(list)
	for node in inp:
		for node2 in inp:
			if node == node2:
				continue
			if manh(node, node2) <= 3:
				edges[node].append(graph.Edge(node2))
				edges[node2].append(graph.Edge(node))
	components = graph.Graph(nodes, edges).connected_components()
	counts = Counter(components.values())
	return len(counts)

def manh(pos1, pos2):
	return sum(abs(pos1[dim] - pos2[dim]) for dim in range(len(pos1)))

def part2(inp):
	return 0

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
