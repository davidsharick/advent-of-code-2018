#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 4)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 0)
		pass

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		if len(line) == 0:
			continue
		presize, size = line.split(":")
		coords = presize.split("@")[1].strip().split(",")
		size = size.strip().split("x")
		claim = (int(coords[0]), int(coords[1]), int(size[0]), int(size[1]))
		o.append(claim)
	return o
	#return readin.int_list_input(inp)

def part1(inp):
	y_max = 0
	x_max = 0
	for claim in inp:
		y_max = max(y_max, claim[1] + claim[3])
		x_max = max(x_max, claim[0] + claim[2])
	o = 0
	for y in range(0, y_max + 1):
		for x in range(0, x_max + 1):
			if in_multiple_claims((y, x), inp):
				o += 1
	return o

def in_multiple_claims(pos, claims):
	y, x = pos
	claims_in = 0
	for claim in claims:
		if (y >= claim[1] and y < claim[1] + claim[3]) and (x >= claim[0] and x < claim[0] + claim[2]):
			claims_in += 1
		if claims_in > 1:
			return True

def part2(inp):
	claims = []
	for idx, claim in enumerate(inp):
		claims.append((2, ((claim[1], claim[1] + claim[3] - 1), (claim[0], claim[0] + claim[2] - 1)), idx))
	for claim in claims:
		accept = True
		for claim2 in claims:
			if claim == claim2:
				continue
			if ranges.range_overlap(claim, claim2):
				accept = False
				break
		if accept:
			return claim[2] + 1

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
