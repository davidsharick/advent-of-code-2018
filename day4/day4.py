#!/usr/bin/env python3

from collections import defaultdict, Counter
from datetime import datetime
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-05 00:55] wakes up"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 240)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 4455)
		pass

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		parts = line.split()
		if len(parts) == 0:
			continue
		#print(parts)
		date = datetime.strptime(parts[0] + " " + parts[1], "[%Y-%m-%d %H:%M]")
		if parts[-1] == "shift":
			o.append((int(parts[3][1:]), date))
		elif parts[-1] == "asleep":
			o.append((-1, date))
		elif parts[-1] == "up":
			o.append((-2, date))
	return o

def part1(inp):
	inp.sort(key = lambda k: k[1])
	guard_sleep_time = Counter()
	curr_guard_id = -1
	sleep_start = -1
	for action, time in inp:
		if action >= 0:
			curr_guard_id = action
			sleep_start = -1
		elif action == -1:
			assert curr_guard_id != -1
			sleep_start = time.minute
		elif action == -2:
			assert sleep_start >= 0
			guard_sleep_time[curr_guard_id] += (time.minute - sleep_start)
	best = guard_sleep_time.most_common(1)[0]
	best_sleep_times = Counter()
	for action, time in inp:
		if action >= 0:
			curr_guard_id = action
			sleep_start = -1
		elif action == -1:
			assert curr_guard_id != -1
			sleep_start = time.minute
		elif action == -2:
			assert sleep_start >= 0
			if curr_guard_id == best[0]:
				for minute in range(sleep_start, time.minute):
					best_sleep_times[minute] += 1
	return best_sleep_times.most_common(1)[0][0] * best[0]

def part2(inp):
	inp.sort(key = lambda k: k[1])
	guard_minute_sleep_time = Counter()
	curr_guard_id = -1
	sleep_start = -1
	for action, time in inp:
		if action >= 0:
			curr_guard_id = action
			sleep_start = -1
		elif action == -1:
			assert curr_guard_id != -1
			sleep_start = time.minute
		elif action == -2:
			assert sleep_start >= 0
			for minute in range(sleep_start, time.minute):
				guard_minute_sleep_time[(curr_guard_id, minute)] += 1
	best = guard_minute_sleep_time.most_common(1)[0][0]
	return best[0] * best[1]

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
