#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """dabAcCaCBAcCcaDA"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 10)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 4)
		pass

def parse_input(inp, p2mode = False):
	return readin.chars_input(inp)

def part1(inp):
	polymer = inp
	i = 0
	while i < len(polymer) - 1:
		curr = polymer[i]
		next = polymer[i + 1]
		if curr.lower() == next.lower() and ((65 <= ord(curr) <= 90) ^ (65 <= ord(next) <= 90)):
			del polymer[i]
			del polymer[i]
			i = max(i - 2, -1) # Needed for some reason
		#print("".join(c for c in polymer), polymer[i], i)
		i += 1
	return len(polymer) # below 9350

def part2(inp):
	best = math.inf
	for char in string.ascii_lowercase:
		new_polymer = inp.copy()
		new_polymer = "".join(c for c in new_polymer)
		new_polymer = new_polymer.replace(char, "").replace(char.upper(), "")
		best = min(best, part1([c for c in new_polymer]))
	return best

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
