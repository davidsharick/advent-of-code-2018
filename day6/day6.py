#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """1, 1
1, 6
8, 3
3, 4
5, 5
8, 9"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 17)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2, max_sum = 32), 16)
		pass

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		parts = line.strip().split(",")
		o.append((int(parts[1].strip()), int(parts[0].strip())))
	return o
	#return readin.int_list_input(inp)
	#return readin.chars_input(inp)

def part1(inp):
	highest_y = max(p[0] for p in inp)
	highest_x = max(p[1] for p in inp)
	c = Counter()
	for y in range(-100, highest_y + 101):
		for x in range(-100, highest_x + 101):
			curr_pos = (y, x)
			dists = Counter()
			for other_pos in inp:
				dist = manhattan(curr_pos, other_pos)
				dists[other_pos] = dist
			best_2 = dists.most_common()[-2:]
			best_2.reverse()
			if best_2[0][1] != best_2[1][1]:
				c[best_2[0][0]] += 1
	
	invalid_positions = set()
	for y in (-10000, 10000):
		for x in range(-10000, 10001):
			curr_pos = (y, x)
			dists = Counter()
			for other_pos in inp:
				dist = manhattan(curr_pos, other_pos)
				dists[other_pos] = dist
			best_2 = dists.most_common()[-2:]
			best_2.reverse()
			if best_2[0][1] != best_2[1][1]:
				invalid_positions.add(best_2[0][0])
	for x in (-10000, 10000):
		for y in range(-10000, 10001):
			curr_pos = (y, x)
			dists = Counter()
			for other_pos in inp:
				dist = manhattan(curr_pos, other_pos)
				dists[other_pos] = dist
			best_2 = dists.most_common()[-2:]
			best_2.reverse()
			if best_2[0][1] != best_2[1][1]:
				invalid_positions.add(best_2[0][0])
	
	commonalities = c.most_common()
	for k, v in commonalities:
		if k not in invalid_positions:
			return v

def manhattan(p1, p2):
	return abs(p1[0] - p2[0]) + abs(p1[1] - p2[1])

def part2(inp, max_sum = 10000):
	highest_y = max(p[0] for p in inp)
	highest_x = max(p[1] for p in inp)
	valid = 0
	for y in range(0, highest_y + 1):
		for x in range(0, highest_x + 1):
			curr_pos = (y, x)
			dist_sum = 0
			for other_pos in inp:
				dist = manhattan(curr_pos, other_pos)
				dist_sum += dist
			if dist_sum < max_sum:
				valid += 1
	return valid

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
