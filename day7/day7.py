#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin."""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), "CABDFE")
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 253)
		pass

def parse_input(inp, p2mode = False):
	nodes = set()
	edges = defaultdict(set)
	for idx, line in enumerate(inp):
		parts = line.split()
		nodes.add(parts[1])
		nodes.add(parts[7])
		edges[parts[1]].add(graph.Edge(parts[7]))
	return nodes, edges
	#return readin.int_list_input(inp)
	#return readin.chars_input(inp)

def part1(inp):
	return "".join(c for c in graph.Graph(inp[0], inp[1]).topo_sort())

def part2(inp):
	max_workers = 5 # 2 for testing
	x = graph.Graph(inp[0], inp[1]).topo_sort(key_sorter = lambda k: 127 - ord(k))
	nodes, edges = inp
	workers = []
	in_edges = Counter()
	for node in nodes:
		if in_edges[node] == 0:
			in_edges[node] = 0 # Ensure the value exists, for iteration purposes
		for adjacent in edges[node]:
			in_edges[adjacent.dest] += 1
	time = 0
	finished_nodes = set()
	while True:
		# increase times
		# remove finished workers
		# add new workers if possible
		new_workers = []
		for worker in workers:
			if worker[0] + 1 == ord(worker[1]) - 4:
				finished_nodes.add(worker[1])
				for edge in edges[worker[1]]:
					in_edges[edge.dest] -= 1
			else:
				new_workers.append((worker[0] + 1, worker[1]))
		
		for k, v in in_edges.items():
			if k in finished_nodes:
				continue
			if v == 0 and len(new_workers) < max_workers:
				if not any(w[1] == k for w in workers):
					new_workers.append((0, k))
		
		workers = new_workers
		time += 1
		if len(finished_nodes) == len(nodes):
			return time - 1
	return time

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
