#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 138)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 0)
		pass

class Node:
	def __init__(self, data):
		num_children = data.pop(0)
		num_metadata = data.pop(0)
		self.child_nodes = []
		self.metadata = []
		self.cached_value = -1
		for _ in range(num_children):
			new_node = Node(data)
			self.child_nodes.append(new_node)
		for _ in range(num_metadata):
			self.metadata.append(data.pop(0))
	
	def calc_meta_sum(self):
		return sum(self.metadata) + sum(node.calc_meta_sum() for node in self.child_nodes)
	
	def calc_value(self):
		if self.cached_value >= 0:
			return self.cached_value
		
		if len(self.child_nodes) == 0:
			self.cached_value = sum(self.metadata)
			return self.cached_value
		else:
			val = 0
			for md in self.metadata:
				idx = md - 1
				if idx >= len(self.child_nodes):
					continue
				val += self.child_nodes[idx].calc_value()
			self.cached_value = val
			return self.cached_value

def parse_input(inp, p2mode = False):
	o = []
	for idx, line in enumerate(inp):
		return [int(c) for c in line.strip().split()]
	return o
	#return readin.int_list_input(inp)
	#return readin.chars_input(inp)

def part1(inp):
	tree = Node(inp)
	return tree.calc_meta_sum()

def part2(inp):
	tree = Node(inp)
	return tree.calc_value()

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
