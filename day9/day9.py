#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.data as data
import utils.frac as frac
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
import utils.ranges as ranges
import utils.input as readin
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """10 players; last marble is worth 1618 points"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = self.testinput#""""""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 8317)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 74765078)
		pass

class Node:
	def __init__(self, value):
		self.value = value
		self.next = self
		self.prev = self
	
	def insert(self, other):
		other.next = self.next
		other.prev = self
		self.next.prev = other
		self.next = other
	
	def remove(self):
		self.next.prev = self.prev
		self.prev.next = self.next

def parse_input(inp, p2mode = False):
	parts = inp[0].strip().split()
	return int(parts[0]), int(parts[6])
	#return readin.int_list_input(inp)
	#return readin.chars_input(inp)

def part1(inp):
	players, last = inp
	curr_player = 0
	scores = Counter()
	used = set()
	used.add(0)
	curr_marble = Node(0)
	curr_value = 1
	start = curr_marble

	while curr_value < last:
		if curr_value % 23 == 0: # Mult of 23
			to_remove = curr_marble.prev.prev.prev.prev.prev.prev.prev
			curr_marble = to_remove.next
			#pr(to_remove.value)
			#pr(curr_marble.value)
			to_remove.remove()
			scores[curr_player] += to_remove.value
			scores[curr_player] += curr_value
		else: # If not mult of 23
			new_marble = Node(curr_value)
			curr_marble.next.insert(new_marble)
			curr_marble = new_marble
		
		curr_value += 1
		curr_player += 1
		curr_player %= players
		#print_ll(start, curr_marble)
	return scores.most_common(1)[0][1]

def part2(inp):
	players, last = inp
	last *= 100 # actually works without needing more efficiency
	curr_player = 0
	scores = Counter()
	used = set()
	used.add(0)
	curr_marble = Node(0)
	curr_value = 1
	start = curr_marble

	while curr_value < last:
		if curr_value % 23 == 0: # Mult of 23
			to_remove = curr_marble.prev.prev.prev.prev.prev.prev.prev
			curr_marble = to_remove.next
			to_remove.remove()
			scores[curr_player] += to_remove.value
			scores[curr_player] += curr_value
		else: # If not mult of 23
			new_marble = Node(curr_value)
			curr_marble.next.insert(new_marble)
			curr_marble = new_marble
		
		curr_value += 1
		curr_player += 1
		curr_player %= players
		#print_ll(start, curr_marble)
	return scores.most_common(1)[0][1]

def print_ll(start, curr):
	curr_node = start.next
	s = str(start.value)
	for _ in range(25):
		s += " "
		s += str(curr_node.value)
		curr_node = curr_node.next
	print("25x\"", s, "\"", curr.value)
	curr_node = start.next
	s = str(start.value)
	while curr_node != start:
		s += " "
		s += str(curr_node.value)
		curr_node = curr_node.next
	print("Forward\"", s, "\"", curr.value)
	curr_node = start.prev
	s = str(start.value)
	while curr_node != start:
		s += " "
		s += str(curr_node.value)
		curr_node = curr_node.prev
	print("Reverse\"", s, "\"", curr.value)

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines(), True)
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
